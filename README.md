**Note:** copy init & appSetup.sh file to Magento folder

**COMMANDS :** 

**root user**

    sudo su 
**init shell script:**

    bash ./init

**Clone Repo**

    git clone https://ankithg03@bitbucket.org/ankithg03/inventrix.git app
    
**change into app folder**

    cd app
    
**Create New Branch**

    git checkout -b feature/{name-of-your-feature/task} Development #ex: git checkout -b feature/sample-module Development
    
**Go back to Magento Directory**
  
    cd .. # or cd {magento-directory} 
    
**run appScript.sh**

    bash ./appSetup.sh
	  
      

